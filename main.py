class Moradia:
    def __init__(self, area, endereco, numero, preco_imovel, tipo='Não especificado'):
        self.area = area
        self.endereco = endereco
        self.numero = numero
        self.preco_imovel = preco_imovel
        self.tipo = tipo

    def gerar_relatorio_preco(self):
        return self.preco_imovel / self.area


class Apartamento(Moradia):
    tipo = 'Apartamento'
    def __init__(self, area, endereco, numero, preco_imovel, preco_condominio, num_apt, num_elevadores, tipo = tipo):
        super().__init__(area, endereco, numero, preco_imovel)
        self.preco_condominio = preco_condominio
        self.num_apt = num_apt
        self.num_elevadores = num_elevadores
        self.andar = int(str(num_apt)[:-1]) if num_apt > 10 else 1
        self.tipo = tipo


    def gerar_relatorio(self):
        return f"{self.endereco} - apt {self.num_apt} - andar: {self.andar} - elevadores: {self.num_elevadores} - " \
               f"preco por m²: {self.gerar_relatorio_preco()} "

